const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const app = require('express')();


// connect to Mongo daemon
mongoose
  .connect(
    'mongodb://mongo:27017/expressmongo',
    { useNewUrlParser: true }
  )
  .then(() => console.log('MongoDB Connected'))
  .catch(err => console.log(err));


// DB schema
const ItemSchema = new mongoose.Schema({
  posting: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  }
});

Item = mongoose.model('item', ItemSchema);
app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(__dirname + '/views'));

app.get("/", (req, res) => {
    Item.find((err, items) => {
      if (err) throw err;
      res.render("index", { items });
    });
  });
app.get('/notification', function(req, res) {
    res.render("notification");
});
app.get('/message', function(req, res) {
    res.render("message");
});


//Post route
app.post('/item/add', (req, res) => {
  const newItem = new Item({
    posting: req.body.posting
  });

  newItem.save().then(item => res.redirect('/'));
});


const port = 3000;
app.listen(port, () => console.log('Server running...'));