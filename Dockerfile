FROM node:10

WORKDIR /app

COPY package.json package.json

RUN npm install

COPY . .
COPY css/styles.css /views/css/
COPY /image /views/image/

EXPOSE 3000

RUN npm install -g nodemon

CMD [ "nodemon", "app.js" ]