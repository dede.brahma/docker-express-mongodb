# Sample docker compose with express and mongodb
This is a demo application illustrating simple twiiter posting application. The demo app is a list in timeline twitter application demo where input posting on the input share post.

# Run app
run the image
```
npm install
```
```
docker-compose up
```
Go to that URL in a web browser
```
http://localhost:80
```
![image](https://raw.githubusercontent.com/DedeBrahma/docker-express-mongodb/master/views/image/result.png)
